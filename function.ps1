function Initialize-Azure {
	#Parameters
	param(
		[Parameter(Mandatory)]
		[string]$ResourceGroupName,

		[Parameter(Mandatory)]
		[string]$StorageAccountName,

		[Parameter(Mandatory)]
		[string]$WebAppName,

		[Parameter(Mandatory)]
		[string]$KeyVaultName,

		[Parameter(Mandatory)]
		[string]$FunctionAppName1,

		[Parameter(Mandatory)]
		[string]$FunctionAppName2,

		[Parameter(Mandatory)]
		[string]$ServicePlanName,

		[Parameter()]
		[string]$Delete
	)
	process {
		#Connecting the Azure Account
		Connect-AzAccount
		#If parameter Delete is equal to yes delete everything
		if ($Delete.ToLower().Equals("yes")) {
			Remove-AzStorageAccount -ResourceGroupName $ResourceGroupName -AccountName $storageAccountName -Force
			Remove-AzWebApp -ResourceGroupName $ResourceGroupName -Name $WebAppName -Force
			Remove-AzAppServicePlan -ResourceGroupName $ResourceGroupName -Name "ServicePlan" -Force
			Remove-AzKeyVault -ResourceGroupName $ResourceGroupName -Name $KeyVaultName -Force
            az keyvault purge --name $KeyVaultName 
			Remove-AzFunctionApp -ResourceGroupName $ResourceGroupName -Name $FunctionAppName1 -Force
			Remove-AzAppServicePlan -ResourceGroupName $ResourceGroupName -Name "WestEuropePlan" -Force
			Remove-AzFunctionApp -ResourceGroupName $ResourceGroupName -Name $FunctionAppName2 -Force
			Remove-AzAppServicePlan -ResourceGroupName $ResourceGroupName -Name $ServicePlanName -Force
            Get-AzResourceGroup -Name $ResourceGroupName | Remove-AzResourceGroup -Force
		}
		else {
			#Creating the Resource Group
            $resourcegroup = New-AzResourceGroup -Name $ResourceGroupName -Location "West Europe"

			#Creating the Storage Account
			$StorageAccount = Get-AzStorageAccount -Name $StorageAccountName -ResourceGroupName $ResourceGroupName -ErrorAction SilentlyContinue

			#Checking if the Storage Account exists
			if ($StorageAccount -eq $null) {
				$storage = New-AzStorageAccount -ResourceGroupName $ResourceGroupName -StorageAccountName $StorageAccountName -Location "westeurope" -SkuName Standard_LRS -Kind StorageV2
			}
			else {
				#If it's found it will prompt to ask to recreate the resource.
				Write-Host "$StorageAccountName already exist"
				$lastValidation = Read-Host "Do you wish to recreate resource? (YES/ NO)"
				if ($lastValidation.ToLower().Equals("yes")) {
					Remove-AzStorageAccount -ResourceGroupName $ResourceGroupName -AccountName $storageAccountName -Force
					#Start-Sleep -s 60
					#pentru testare
					$storage = New-AzStorageAccount -ResourceGroupName $ResourceGroupName -StorageAccountName $StorageAccountName -Location "westeurope" -SkuName Standard_LRS -Kind StorageV2
				}
				else {
					Write-Host "Aborded. Nothing was recreated." -ForegroundColor Cyan
				}
			}


			$WebApp = Get-AzWebApp -Name $WebAppName -ResourceGroupName $ResourceGroupName -ErrorAction SilentlyContinue

			#Checking if the Web App exists
			if ($WebApp -eq $null) {
				$webapp = New-AzWebApp -ResourceGroupName $ResourceGroupName -Name $WebAppName -Location "West Europe" -AppServicePlan "ServicePlan"
			}
			else {
				#If it's found it will prompt to ask to recreate the resource.
				Write-Host "$WebAppName already exist"
				$lastValidation = Read-Host "Do you wish to recreate resource? (YES/ NO)"
				if ($lastValidation.ToLower().Equals("yes")) {
					Remove-AzWebApp -ResourceGroupName $ResourceGroupName -Name $WebAppName -Force
					Remove-AzAppServicePlan -ResourceGroupName $ResourceGroupName -Name "ServicePlan" -Force
					#Start-Sleep -s 60
					#pentru testare
					$webapp = New-AzWebApp -ResourceGroupName $ResourceGroupName -Name $WebAppName -Location "West Europe" -AppServicePlan "ServicePlan"
				}
				else {
					Write-Host "Aborded. Nothing was recreated." -ForegroundColor Cyan
				}
			}


			$VaultKey = Get-AzKeyVault -Name $KeyVaultName -ResourceGroupName $ResourceGroupName -ErrorAction SilentlyContinue

			#Checking if the Vault Key exists
			if ($VaultKey -eq $null) {
				$vaultkey = New-AzKeyVault -VaultName $KeyVaultName -ResourceGroupName $ResourceGroupName -Location 'West Europe'
			}
			else {
				#If it's found it will prompt to ask to recreate the resource.
				Write-Host "$KeyVaultName already exist"
				$lastValidation = Read-Host "Do you wish to recreate resource? (YES/ NO)"
				if ($lastValidation.ToLower().Equals("yes")) {
					Remove-AzKeyVault -ResourceGroupName $ResourceGroupName -Name $KeyVaultName -Force
                    az keyvault purge --name $KeyVaultName 
					#Start-Sleep -s 60
					#pentru testare
					$vaultkey = New-AzKeyVault -VaultName $KeyVaultName -ResourceGroupName $ResourceGroupName -Location 'West Europe'
				}
				else {
					Write-Host "Aborded. Nothing was recreated." -ForegroundColor Cyan
				}
			}


			#Creating a service plan to host a Function App
			$ServicePlan = New-AzAppServicePlan -ResourceGroupName $ResourceGroupName -Name $ServicePlanName -Location "West Europe" -Tier "Basic" -NumberofWorkers 2 -WorkerSize "Small"

			$FunctionApp1 = Get-AzFunctionApp -Name $FunctionAppName1 -ResourceGroupName $ResourceGroupName -ErrorAction SilentlyContinue
			
			#Checking if the Consumption Function App exists
			if ($FunctionApp1 -eq $null) {
				$functionapp1 = New-AzFunctionApp -Name $FunctionAppName1 -ResourceGroupName $ResourceGroupName -Location "West Europe" -StorageAccount $StorageAccountName -Runtime "PowerShell"
			}
			else {
				#If it's found it will prompt to ask to recreate the resource.
				Write-Host "$FunctionAppName1 already exist"
				$lastValidation = Read-Host "Do you wish to recreate resource? (YES/ NO)"
				if ($lastValidation.ToLower().Equals("yes")) {
					Remove-AzFunctionApp -ResourceGroupName $ResourceGroupName -Name $FunctionAppName1 -Force
					Remove-AzAppServicePlan -ResourceGroupName $ResourceGroupName -Name "WestEuropePlan" -Force
					#Start-Sleep -s 60
					#pentru testare
					$functionapp1 = New-AzFunctionApp -Name $FunctionAppName1 -ResourceGroupName $ResourceGroupName -Location "West Europe" -StorageAccount $StorageAccountName -Runtime "PowerShell"
				}
				else {
					Write-Host "Aborded. Nothing was recreated." -ForegroundColor Cyan
				}
			}

			$FunctionApp2 = Get-AzFunctionApp -Name $FunctionAppName2 -ResourceGroupName $ResourceGroupName -ErrorAction SilentlyContinue
			
			#Checking if the Service Plan Hosted Function App exists
			if ($FunctionApp2 -eq $null) {
				$functionapp2 = New-AzFunctionApp -Name $FunctionAppName2 -ResourceGroupName $ResourceGroupName -PlanName $ServicePlanName -StorageAccount $StorageAccountName -Runtime "PowerShell"
			}
			else {
				#If it's found it will prompt to ask to recreate the resource.
				Write-Host "$FunctionAppName2 already exist"
				$lastValidation = Read-Host "Do you wish to recreate resource? (YES/ NO)"
				if ($lastValidation.ToLower().Equals("yes")) {
					Remove-AzFunctionApp -ResourceGroupName $ResourceGroupName -Name $FunctionAppName2 -Force
					Remove-AzAppServicePlan -ResourceGroupName $ResourceGroupName -Name $ServicePlanName -Force
					#Start-Sleep -s 60
					#pentru testare
                    $ServicePlan = New-AzAppServicePlan -ResourceGroupName $ResourceGroupName -Name $ServicePlanName -Location "West Europe" -Tier "Basic" -NumberofWorkers 2 -WorkerSize "Small"
					$functionapp2 = New-AzFunctionApp -Name $FunctionAppName2 -ResourceGroupName $ResourceGroupName -PlanName $ServicePlanName -StorageAccount $StorageAccountName -Runtime "PowerShell"
				}
				else {
					Write-Host "Aborded. Nothing was recreated." -ForegroundColor Cyan
				}
			}
		}
	}
}