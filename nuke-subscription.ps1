Connect-AzAccount

$allSubs = Get-AzSubscription

$allSubs | Sort-Object Name | Format-Table -Property Name,SubscriptionId,State


$theSub = Read-Host "Enter the subscriptionId you want to clean"

Write-Host "You selected the subscription." -ForegroundColor Cyan
Get-AzSubscription -SubscriptionId $theSub | Get-AzSubscription

$allRG = Get-AzResourceGroup

foreach ($g in $allRG) {

	Write-Host $g.ResourceGroupName -ForegroundColor Yellow
	Write-Host "------------------------------------------------------`n" -ForegroundColor Yellow

	$query = "`$filter=resourceGroup eq '{0}'" -f $g.ResourceGroupName
	$allResources = Get-AzResource -ODataQuery $query

	if ($allResources) {
		$allResources | Format-Table -Property Name,ResourceName
	} else {
		Write-Host "-- empty--`n"
	}
	Write-Host "`n`n------------------------------------------------------" -ForegroundColor Yellow
}

$lastValidation = Read-Host "Do you wish to delete ALL the resouces previously listed? (YES/ NO)"

if ($lastValidation.ToLower().Equals("yes")) {

	foreach ($g in $allRG) {

		Write-Host "Deleting " $g.ResourceGroupName
		Remove-AzResourceGroup -Name $g.ResourceGroupName -Force

	}
} else {
	Write-Host "Aborded. Nothing was deleted." -ForegroundColor Cyan
}
