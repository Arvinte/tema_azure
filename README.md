# Tema Azure

---

## Fisiere:
- _create_resources.ps1_ creeaza resursele in mod automat cu parametrii predefiniti
- _nuke-subscripiton.ps1_ distruge toate resursele de pe un subscription
- _script_automate_create_resources.ps1_ creeaza resursele dar cu input de la user
- _function.ps1_ creeza o functie pentru a crea resurse, care poate fi apelata in Powershell

## Pasi pentru implementarea functiei:
1. Se deschide Powershell in folderul cu fisierul _function.ps1_
2. Se da comanda ```. .\function.ps1```
3. Se apeleaza functia cu ```Initialize-Azure```
4. Se pot introduce parametrii direct la apelarea functiei sau dupa apelarea functiei in prompt:
   - ```-ResourceGroupName "Resource Group Name"```
   - ```-StorageAccountName "Storage Account Name"```
   - ```-WebAppName "Web App Name"```
   - ```-KeyVaultName "Key Vault Name"```
   - ```-FunctionAppName1 "Function App 1 Name"```
   - ```-FunctionAppName2 "Function App 2 Name"```
   - ```-ServicePlanName "Service Plan Name"```
5. Pentru _Delete all resources_ se apeleaza functia cu ```Initialize-Azure -Delete yes```