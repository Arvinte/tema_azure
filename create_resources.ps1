Connect-AzAccount
Set-AzContext -SubscriptionId '0ae52849-7605-43a5-9ad0-0b172527a18e'
New-AzResourceGroup -Name "Test" -Location "West Europe"
New-AzWebApp -ResourceGroupName "Test" -Name "ArvinteTestInternship" -Location "West Europe" -AppServicePlan "ServicePlan"
New-AzStorageAccount -ResourceGroupName 'Test' -Name 'testarvinteinternship' -SkuName 'Standard_LRS' -Location 'West Europe'
New-AzKeyVault -VaultName 'VaultTestArvinte' -ResourceGroupName 'Test' -Location 'West Europe'
New-AzAppServicePlan -ResourceGroupName "Test" -Name "PlanTest" -Location "West Europe" -Tier "Basic" -NumberofWorkers 2 -WorkerSize "Small"
New-AzFunctionApp -Name functieconsum `
 	-ResourceGroupName Test `
 	-Location westeurope `
 	-StorageAccount testarvinteinternship `
 	-Runtime PowerShell
New-AzFunctionApp -Name functiehost `
 	-ResourceGroupName Test `
 	-PlanName PlanTest `
 	-StorageAccount testarvinteinternship `
 	-Runtime PowerShell
Read-Host -Prompt "All done! Press Enter to exit"
